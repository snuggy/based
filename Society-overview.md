# Society

expand your awareness
consider the culture you're in

with people all around you
	beliefs all resulting from learning
    family, friends and communities
    overarching culture -- shaped throughout history


differing opinions on everything
	opinions are the cheapest commodity on earth
    someone, somewhere with an exact opposite viewpoint
	you don't need to agree with them, just accept people have them
we could endlessly argue, or simply
	be the best versions of outselves
	sending love and kindness to others
	be curious their experiences



we become what we think about


so I don't send any hatred to the porn industry, or it's users or independent creators
but I will do everything I can to stop it, peacefully,

faith end soon
must based truth
	porn industry
		lie after lie
		paying researchers and reporters
		health risks
        covering up
		trafficing


## moral argument
    abuse from professional porn industry producers
    interviews, both professional and casual
        "obviously you don't feel good about yourself"

    those who support pornography
        often support sex worker rights
        but lets take a couple of steps back

    mother 
        fortunate to have a nice body
        pushed sex work
    starving
    experiencing homelessness
        like many other people
    stressing
        no longer the 'it girl'
        inflation has meant that she has to start fulfilling
        lust filled demands of consumers
        not comfortable with

        her and her baby will starve

XXX

in the same breath
    fed using food produced by developing nations
    developing nations feel the brunt
    backbreaking work
        often in slavery
        or if not, for slave-esque wages
    

    consumptive collective habits
        we post and view porn online
        eat stuff grown by unhappy people 
            surrounded by others being exploited
        out of a plastic packet
        moved by trucks
        
    both symptoms of a disease called consumerism
        slowly, inevitably destroying the planet



isn't an inditement for you
or for anyone
    because I did it too
    leave a trap when you know you're in one
    end of the book has guides for easily growing your own food
        making the world a bit better
            if you have a surplus, sharing food with neighbours
                getting them on board
            when has top down revolution ever worked, change starts with you
            


consumerism overarching theme
because we've been conditoned into identifying with it

    future with your kids
        perplexed amazement
        'ok boomer'
        'keep scrolling zoomer'


## scrolling

it's pretty wild
attention economy
    weird concept
you know it's bad


remember very little
    newsworthy events from five years ago (2017)
        remember one or two
        makes you feel sad
    what influencers were doing
        probably something shock or sex related
    even what your friends were doing
    
    limbic system
    in many places,
	    economic system incentivises it


    special moments with loved ones
        like my sister's birthday
        or going to a really fun party

I'm not going to tell you things you already know
    - really bad for your brain
    - cost you a lot of time
 because everyone already does that, so I'll tell an odd story instead




so basically, 
sketcky things are happening


MCKEIGAN STORY



	
	
	
**complexity**
	africa
		
	america
		people are pleasant
		consumerism
		exceptionalism
		because they lack perspective
		decline
	russia
		
		
	china
		censorship
		dictatorship
		ccp
	colombia
		poverty
		homelessness
		friendly
		suffering
		what matters

	[[Morality]]
	
	

---
after all, we live in a...



































normal
	stigma
	understand bad
	science
	withdrawal
	normalisation
	smoking

tide is turning


built upon porn
	normalising

reasonings



media
social media

## oddity

I was fasting for four days once and urgently had coconut water because I needed potassium for my nervous system, but didn't realise caffeine had been added.

all nighter
started running on the beach

muttering money
conversation


donald mckeigann

---


questioning
	[[based/concepts/stigma]]
	[[Internet]]
	[[manipulation]]
		[[media]]
		[[Social media]]
	system
		[[industrial]]
		[[profit]]

**hopeful**
	tide is turning [[based/concepts/stigma]]
		culture		
		no nut november	
		coomer meme


[[based/book/old/complexity]]
[[culture]]
[[industrial]]
[[Institutions]]
[[Internet]]
	marketing guff
[[manipulation]]
[[Morality]]
[[profit]]
[[muslim]]

[[sex trafficing]]
[[Slavery]]



hermetic answer
    whatever helps you to produce more

In the Poetic Worldview, the highest moral goal is creation. That can be:

    affecting the world,
    improving what is around you,
    having children,
    making money not to spend it on pleasures, but to make something new and great with it,
    writing or making something useful or edifying for others,
    clearing up misconceptions that get in other people's way in accomplishing these things.


distractions

for me, it's ponytails
	sex thing
	aesthetically pleasing
		swish swish
		when a women puts her hair in a ponytail she's getting something done
I don't really know why I find them attractive




---

## media

    need to be aware of danger to stay alive

 In a study carried out by the American Psychological Association, half of all adults are suffering from stress caused by news addiction.

willpower
helpless

The key message here is: Reading the news doesn’t help you change the world for the better.

Perhaps you think by following every development of an unfolding disaster, you’re showing your empathy for the sufferers. By agonizing along with them, you feel as if you’re showing your humanity. And at times like that, humanity is needed more than ever, isn’t it?

Unfortunately, the only people you’re helping are the media outlets and their sponsors. With your endless clicks, you’re filling the pockets of those who benefit from the media’s revenue model. That’s it. And in fact, isn’t there something quite wrong with consuming every gruesome detail of others’ misfortune?


I'd invite you to question media, as a whole, too 

it's a massive industry
with many people involved in it


driven
attention economy
    somewhat dystopic
    many implications
    
largely driven by shock or sex

news
    extremely strange
        more pronouced when looking at news in another language
            programmed
            hyperstimulated

        sells you into being 'informed'
            desire you to watch more tv
        
    journalists
        extremely important for free democracies
        ethics
            advance their careers
            opinions
            interests of multinational corporations


hundreds of topics
only a shallow understanding

The ideal, then, is for all of us to become more like that second surgeon. People who know and care about their area of expertise, not distracted, irritable news addicts desperate to get back to their phone screens.

 if you don’t read the news, how can you still be an informed citizen and take part in democracy?

Well, quite simply, just like many other people did before the advent of mass media. How did active citizens manage to stay informed during the early democracy of Ancient Greece? They thought deeply and debated one another.


There’s a clear difference between real journalism and “the news.” Journalism requires real digging, providing background information, and being able to tell complex stories. It doesn’t simplify or cut corners. It requires skill, expertise, and time.

--- news


celebrities
    sex
    public comments 
    endless numbers of people supporting them



influencers
    sex
    brands seeking to make their way to you




    
    
    
    
    
    

print
news
entertainment
television
social media

    
    

passive consumption of media
		print
		television
		social media
	people involved with portraying it
	celebrities
	influencers
	support them
		camera
		script writers
    incentivised
	sex sells
	attention economy
		shock value		
		celebrities		
			relevance
				
		
		
		


news incentivised
    keep you viewing their side of the story
        not really sure who 'they' are
        people
        economic system incentivises
        love people
        don't like the system

    
sketchy things are happening










but I do know that

everything we do is a result of learning
	
		family
		traditions
		institutions
			schools
			religious
			
		

			
everyone is allowed an opinion
don't have to agree
accept them
cheapest commodity on earth
but so is yours

opposite
	learning
	life is hard for everyone
	convince people that you way of living was better
	live your life
	be the best you can be
	kind, compassionate
	osmosis

	anime meme
		parents walk in
		sex scene is happening
		how often does it happen and you're unconscious of it?



**a moral argument**
	porn industry corruption
	industrial society




---
after all, we live in a...




with people all around you
	all resulting from learning
differing opinions on everything
	opinions are the cheapest commodity on earth
	you don't need to agree with them, just accept people have them
we could endlessly argue, or simply
	be the best versions of outselves
	send love and kindness
	be curious to others experiences

I've met a lot of people


we become what we think about
so I don't send hatred to the porn industry, it's users or independent creators
but I will do everything I can to stop it


though everyone is allowed opinions
based on truth and justice
fall, countless examples

	porn industry
		lie after lie
		paying researchers and reporters
		trafficing
		health risks




normal
	stigma
	understand bad
	science
	withdrawal
	normalisation
	smoking

tide is turning


built upon porn
	normalising

reasonings



media
social media
