# Stigma

Stigma is labelling a person and overlooking everything else.

Like calling someone 'homeless' instead of 'in homelessness'

Of course, but we're rarely considering it.




David Chappelle compares 'poor' and being broke.
One has a person.


So consider Sarah, who has schizophenia, a condition where people see, hear and think about things that aren't there. At times it's really scary, but thanks to therapy and medication most with schizophenia live completely normal lives.

We often hearing about crazed 'schizophenics' causing violence and devestation, even going as far as calling erratic behaviour 'schizo', but these terms reduce someone's complexity to a label.


It only happens occasionally, but Sarah overhears someone calling their mates 'schizo'. 
She honestly not mad about it, but it's just frustrating because she's knows what living with schizophrenia is like, yet it's some 'crazy' label.

"... and like, people with schizophenia aren't violent either, and you can control symptoms with medication. [...] you're going around and you're dealing with shadow people *only sometimes though its chill* and everyone's weirded out by it."

Sarah has her own hobbies, humour, friends, skincare routine and subtle joys, but finds everything's overlooked if people find out about her condition.

She's honestly enjoying her job -- she explained it to me and it's very cool -- but it's been difficult, not because of her conditon, but her coworker's beliefs about her.

Sarah had a psychotic episode some months back because of a XXX XXX and XXXX, and couldn't work for around a month and a half. It happened with friends, but news travels and everything's different suddenly different at work.

Nobody really asks her if she's okay, and sort of prod when asking her about it. Yep, it sucks. It's impacting her job -- previously great ideas are always questioned in meetings. "Just checking in" and just how it feels around work.

All these misconceptions lead people to 'self-stigmatise', where people start taking others opinions in and second-guessing themselves, becoming XXX



It's been a couple of months and Sarah's sort of treating it like a dark comedy. "The psychosis was terrifying, but at least it went away. I guess I've just gotta show everyone I'm normal."

"Have you thought about leaving?

"Yeah every day haha"


## Addiction

Methamphetamine is addictive because it gives infinitely more dopamine than would normally be possible.

Right now someone is discussing treatment options with their doctor, who'll be pointing them to various medications, therapies and recovery centres.

As a society we view people addicted to substances -- alcohol, nicotine, meth, crack, others -- as needing help, not shunning them.



Pornography is addictive too, but ask for the same and the conversation won't go that way.



Why is this?

Let's ask three questions about different drugs. Are they,

1. Harmful?
2. Addictive?
3. Normalised?

Well, for starters -- methamphetamine vapor is certainly very harmful^[1], and extremely addictive, partly owing to it's harm.
So it's not normalised, and we treat it as a public health issue.

[1] well, we do prescribe swallowing it.

Another drug, nicotine, works similarly. It's an extremely quick acting addictive poison, especially when inhaled. Many have trouble quitting it.
Vaping's becoming more popular, but it's not normalised -- so it's a health issue.


From what I understand, porn is harmful too because we simply can't handle the dopamine spike from endless novelty, and there's heaps of research on how it's desensitising our brains.
We could argue it's extent, but it's just how supernormal dopamine spiking works.

A quote,
	



Wel

Most doctors use pornography, and when I mentioned that "porn is probably impacting the mental health of your patients" to my doctor, and after briefly mentioning some of the neuroscience I quickly found myself in the 'you need to leave' vibe.

This is somewhat odd, because surely they should be most aware of the health risks. Same with smoking, but many nurses smoke too.

We'll get into the reasons why soon, but I'm just illustrating that awareness of the health risks doesn't help people to quit.

o






[[breathe]]
