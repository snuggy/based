# Stigma

Stigma is labelling a person and overlooking everything else.

We do this all the time, like calling someone 'homeless' instead of 'in homelessness'.

$$\\[0.5em]$$

One is a person.




$$\\[2em]$$

Mental conditions are often stigmaitised too.  Consider Sarah who has schizophrenia, a conditon where people see, hear and think about things that aren't there.

What do you imagine?

$$\\[2em]$$

We're often hearing about crazed 'schizophenics' causing violence and devestation, even going as far as calling erratic behaviour 'schizo', but these terms reduce someone's complexity to a label.

News, media and third-hand stories condition us into believing that people with schizophenia are violent and unpredicatable. 

$$\\[2em]$$


It only happens occasionally, but Sarah overhears someone calling their mates 'schizo' on her way to work.  
She honestly not mad about it, but it's just frustrating because she's knows what living with schizophrenia is like, yet it's some 'crazy' label.


*"... and like, people with schizophenia aren't violent either, and you can control symptoms with medication. [...] you're going around and you're dealing with shadow people *only sometimes though its chill* and everyone's weirded out by it."*


Sarah has her own hobbies, humour, friends, skincare routine and subtle joys, but finds everything's overlooked if people find out about her condition.


Sarah had a psychotic episode some months back because of a XXX XXX and XXXX, and couldn't work for around a month and a half. It happened with friends, but news travels and suddenly everything's different at work.

Nobody really asks her if she's okay, and sort of prod when asking her about it. Yep, it sucks. It's impacting her job -- previously great ideas are always questioned in meetings. "Just checking in" and just how it feels around work.

All these misconceptions lead people to 'self-stigmatise', where people start taking others opinions in and second-guessing themselves, becoming XXX

$$\\[0.1em]$$

It's been a couple of months and Sarah's sort of treating it like a dark comedy. "The psychosis was terrifying, but at least it went away. I guess I've just gotta show everyone I'm normal."

"Have you thought about leaving?

"Yeah every day haha"


$$\\[2em]$$

We stigmatise few addictions -- from a public health standpoint at least -- because we understand that someone addicted to nicotine, crack or meth as having fallen into a trap and as needing help getting out.

Treating these addictions is done through various medications, therapies and recovery centres, and we understand that shunning them probably won't help.


$$\\[0.15em]$$

Pornography is addictive too, but ask for the same from your doctor and the conversation won't go that way.

$$\\[0.15em]$$

Why is this?

Well, 

age starting
	effects are subtle
modern: progress
	different technology
socially normal


question it
	incident
		pied
		escalation
	no nut november challenge



two thoughts
	everyone else does it
	stopping means we have a problem
	stopping means that we've forever labelled as an addict

		rationalise
			everyone does it!
			benefits outweigh the risks
			(but since nobody's brain can handle porn isn't everyone addicted?)


	"I have a problem"
		your addicted
		treatment
		streak counting
		hard
		fail
		shame
		reinforced
			others
				"isn't really a problem for me"
				online quitting communities



$$\\[0.5em]$$

*Almost everyone uses pornography though*, so quitting is something

A quote,
	We have created a



Wel

Most doctors use pornography, and when I mentioned that "porn is probably impacting the mental health of your patients" to my doctor, and after briefly mentioning some of the neuroscience I quickly found myself in the 'you need to leave' vibe.

This is somewhat odd, because surely they should be most aware of the health risks. Same with smoking, but many nurses smoke too.

We'll get into the reasons why soon, but I'm just illustrating that awareness of the health risks doesn't help people to quit.

o
























So consider Sarah, who has schizophenia, a condition where people see, hear and think about things that aren't there. At times it's really scary, but thanks to therapy and medication most with schizophenia live completely normal lives.

We often hearing about crazed 'schizophenics' causing violence and devestation, even going as far as calling erratic behaviour 'schizo', but these terms reduce someone's complexity to a label.

$$\\[0.15em]$$

It only happens occasionally, but Sarah overhears someone calling their mates that on her way to work
She honestly not mad about it, but it's just frustrating because she's knows what living with schizophrenia is like, yet it's some 'crazy' label.

"... and like, people with schizophenia aren't violent either, and you can control symptoms with medication. [...] you're going around and you're dealing with shadow people *only sometimes though its chill* and everyone's weirded out by it."

Sarah has her own hobbies, humour, friends, skincare routine and subtle joys, but finds everything's overlooked if people find out about her condition.

She's honestly enjoying her job -- she explained it to me and it's very cool -- but it's been difficult, not because of her conditon, but her coworker's beliefs about her.

Sarah had a psychotic episode some months back because of a XXX XXX and XXXX, and couldn't work for around a month and a half. It happened with friends, but news travels and everything's different suddenly different at work.

Nobody really asks her if she's okay, and sort of prod when asking her about it. Yep, it sucks. It's impacting her job -- previously great ideas are always questioned in meetings. "Just checking in" and just how it feels around work.

All these misconceptions lead people to 'self-stigmatise', where people start taking others opinions in and second-guessing themselves, becoming XXX

$$\\[0.1em]$$

It's been a couple of months and Sarah's sort of treating it like a dark comedy. "The psychosis was terrifying, but at least it went away. I guess I've just gotta show everyone I'm normal."

"Have you thought about leaving?

"Yeah every day haha"
