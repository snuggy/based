# Stigma

Stigma is labelling a person and overlooking everything else.

Like calling someone 'homeless' instead of 'in homelessness'. I'm hoping you understand it's important. Someone's either just one thing, or they're a person as complex as you.



Try thinking about Sarah, who has schizophenia, a condition where people see, hear and think about things that aren't there. At times it's really scary, but thanks to therapy and medication most with schizophenia are living completely normal lives.

We often hearing about crazed 'schizophenics' causing violence and devestation, even going as far as calling erratic behaviour 'schizo', but these terms reduce someone's complexity to a label.


It's never happened while she's walking to work, but Sarah overhears someone calling their mates 'schizo'. It's not like she's mad about it, but it's just frustrating, because she's knows what living with schizophrenia is like, yet everyone thinks she's 'crazy'.

Sarah has her own hobbies, humour, friends, skincare routine and subtle joys, but finds everything's overlooked if people find out about her condition.

She's honestly enjoying her office job -- lowkey, I've seen her work and she's really talented -- but it's been difficult, not because of her conditon, but her coworker's beliefs about her.

Sarah had a psychotic episode some months back because of a XXX XXX and XXXX, and couldn't work for XXX weeks. When she'd recovered, she said "the office's vibe really had changed" -- formerly friendly coworkers were on edge, with hushed tones and occasional glances, with people 'just checking in' before treating her as a person.

Her ideas  -- which were previously applauded -- are now constantly questioned in meetings.

Sarah has to be mindful not letting others get her down. After all, they've usually heard about schizophenia through the media -- movies of deranged killers and unstable people, or highly reported news of people with mental illness committing crimes (rather than the sane, who are committing the majority) -- or stories from people who heard something from someone.

All these misconceptions lead people to 'self-stigmatise', where people start taking others opinions in and second-guessing themselves, becoming XXX



It's been a couple of months and Sarah's sort of treating it like a dark comedy. "The psychosis was terrifying, but at least it went away. I guess I've just gotta show everyone I'm normal."

"Have you thought about leaving?

"Yeah every day haha"


## Addiction

Methamphetamine is addictive because it gives infinitely more dopamine than would normally be possible.

Right now someone is discussing treatment options with their doctor, who'll be pointing them to various medications, therapies and recovery centres.

As a society we view people addicted to substances -- alcohol, nicotine, meth, crack, others -- as needing help, not shunning them.



Pornography is addictive too, but ask for the same and the conversation won't go that way.



Why is this?

Let's ask three questions about different drugs. Are they,

1. Harmful?
2. Addictive?
3. Normalised?

Well, for starters -- methamphetamine vapor is certainly very harmful^[1], and extremely addictive, partly owing to it's harm.
So it's not normalised, and we treat it as a public health issue.

[1] well, we do prescribe swallowing it.

Another drug, nicotine, works similarly. It's an extremely quick acting addictive poison, especially when inhaled. Many have trouble quitting it.
Vaping's becoming more popular, but it's not normalised -- so it's a health issue.


Porn is harmful as well because we simply can't handle the dopamine spike from endless novelty, and there's heaps of research on how it's desensitising our brains.
We could argue it's extent, but it's just how supernormal dopamine spiking works.

A quote,



---

## Pornography

Since everyone uses pornography, we

Most doctors use pornography, and when I mentioned that "porn is probably impacting the mental health of your patients" to my doctor, and after briefly mentioning some of the neuroscience I quickly found myself in the 'you need to leave' vibe.

This is somewhat odd, because surely they should be most aware of the health risks. Same with smoking, but many nurses smoke too.

We'll get into the reasons why soon, but I'm just illustrating that awareness of the health risks doesn't help people to quit.

o






[[breathe]]
