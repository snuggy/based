# Welcome,

this book is for you.
no matter how much pornography you use, or how you were exposed.
or quitting or supporting someone else. or just interested

please enjoy



probably exposed young; average age is nine.
"woah" we think, but "well, that sounds about right".
or it was long ago, and has become a fact of life since -- but is it normal?

well, yes -- almost everyone uses pornography. so it's *everywhere*, but perhaps not normal. 
after all sometimes sites scare you with health risks, or start discussing relationship problems 

most agree with them, but "porn feels good" and you're not worried about it

or perhaps you are, but struggling using willpower -- anything involving counting, XXX XXXX XXXX XXXX XXX -- or are concerned about your partner.


well, it's lovely meeting you. welcome to the based method.

---
based
   something built upon

  *slang*: something agreeable yet contrary

---

it's both, a foundation and hot take

you'll quit pornography painlessly, immediately and permanently after reading.

but only *after* reading, so please //do not// try cutting down or reducing your porn usage before you've read the book

you'll understand why after finishing the book, but this is really important



how trippy you're reading this though. globally instantly, "hello"

computers were size of rooms, once you didn't even have a phone
you know where I'm going with it

now pornography. 




Anywhere, about anything, for as long as you'd like.


If you've never considered quitting, this book gives some ideas to consider. I won't be hard-headedly pressuring, forcing or scaring you into quitting -- such approaches don't work, and doing so would be rude -- and you *should not* reduce the amount of pornography you use while reading. This might sound absurd, but it's very important and you'll understand why.

If you've tried quitting, you've done so using willpower -- anything involving counting, blocking, rewarding, dieting or punishing -- only making you more fearful and cautious of porn. Willpower is finite, so resisting 'urges' under stress drives you to try releving your stress using porn; creating a shame powered cycle when you relapse.

Most who've tried quitting -- including myself -- have gone through this, and it sucks. It rests on the assumption that 'less porn equals less desire', which is sometimes fed by reading lots of science-filled sites.
Knowing all the science just makes you more fearful that you're hooked.

It's like the smoker telling you everything terrible about cigarettes but who can't quit.
Which certainly isn't a fault of them, just how we've been conditioned to think about addiction.



This method works differently through reframing porn. It's like a guidebook; you shouldn't follow it blindly; instead, asking if it's true (and if what you currently believe is too).

If you choose to quit after reading this book, you'll honestly find it enjoyable.  You won't need willpower or streak counting. You'll feel free right from the start.



**No skipping chapters.

**Keep using pornography as you have until you've finished the book.**



If you've already quit, are on a sizeable streak or in the fasting days of a porn diet, just keep doing what you're doing. Otherwise, keep using as much as you'd like.

**These are serious instructions:** It's important you've considered everything before quitting.





Well, let's first consider why quitting pornography is difficult.


## Fear

like when the internet barely works.
or when you can't control it.
or when you feel like you're hooked for life.


Or, when you're simply fine with how much you use because you like the way it makes you feel, and it's just a simple stress relief tool. 

After all, you'd sleep better the night before your exam. Your meetings won't go as smoothly. You'll have nothing to do, and you won't be able to concentrate either. It's just, nice, having it there -- y'know?
[[sleep]] [[concentration]] [[relaxation]] [[boredom]]


I'm not here to put your back up against the wall, but to give you some things to consider.

So please reflect, *when did you decide to become a porn user?*
As in, making the positive decision that you wanted to use porn for the rest of your life.

That you, 
- must have, or needed pornography to masturbate
- had to have porn induced fantasies, even with partners
- always used it after getting home
- couldn't sleep without it
- couldn't concentrate or handle stress without it.


I'm going to be arguing that you've been lured into this, and that there's no such thing as a porn addict, just varying degrees of addiction to a supernormal stimulus that nobody can handle.

If you believe it's just a habit, ask yourself why other habits -- some of which are very enjoyable -- are easy to break,  but one feeling awful and costing energy, time and libido is difficult.

Or, if you feel you're enjoying it -- why can't you just 'take it or leave it' like anything else? Why does panic set in at the thought of not having it?

Unfortunately, we go about the process of kicking it all back to front.
Fear prevents us from quitting because we believe we'll be going through a period of misery and craving when we stop.

Slipping up or quitting and deciding to continue using reinforces beliefs about what life's like without it.

Instead, we'll be approaching it in the same way you'd drop a rope.




Before discussing our own beliefs, let's start by first discussing porn's stigma.

[[based/book/stigma]]