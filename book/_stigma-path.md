# Stigma Path

Methamphetamine
	Treatment

Meth is addictive
Meth causes harm
Meth isn't normal



nicotine
	nicotine is addictive
		though we didn't used to know this
	nicotine causes harm
		poison,
	nicotine isn't normal


pornography
	pornography is addictive
		fits the criteria
		"plenty of things are addictive"
        pornography causes harm
		harms aren't immediately apparent
		depression, anxiety
        pornography isn't normal
		but it is normal

	why?
		fully normalised
			similar to nicotine
		harms aren't immediately apparent
		we start using porn early in life


labelling
	[[../concepts/fear]]
		of being a porn addict
			part of your identity
			partner
			enjoy something that everyone else is enjoying
			normal
			forever

		easier even if you do suspect it,
		pretend it's not happening
		wait for an incident
			pied

		cognitive dissonance


	what would it mean being a porn addict?
		that you'd have a problem
