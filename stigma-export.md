
So consider Sarah, who has schizophenia, a condition where people see, hear and think about things that aren't there. At times it's really scary, but thanks to therapy and medication most with schizophenia live completely normal lives.

We often hearing about crazed 'schizophenics' causing violence and devestation, even going as far as calling erratic behaviour 'schizo', but these terms reduce someone's complexity to a label.

$$\\[0.15em]$$

It only happens occasionally, but Sarah overhears someone calling their mates that on her way to work
She honestly not mad about it, but it's just frustrating because she's knows what living with schizophrenia is like, yet it's some 'crazy' label.

"... and like, people with schizophenia aren't violent either, and you can control symptoms with medication. [...] you're going around and you're dealing with shadow people *only sometimes though its chill* and everyone's weirded out by it."

Sarah has her own hobbies, humour, friends, skincare routine and subtle joys, but finds everything's overlooked if people find out about her condition.

She's honestly enjoying her job -- she explained it to me and it's very cool -- but it's been difficult, not because of her conditon, but her coworker's beliefs about her.

Sarah had a psychotic episode some months back because of a XXX XXX and XXXX, and couldn't work for around a month and a half. It happened with friends, but news travels and everything's different suddenly different at work.

Nobody really asks her if she's okay, and sort of prod when asking her about it. Yep, it sucks. It's impacting her job -- previously great ideas are always questioned in meetings. "Just checking in" and just how it feels around work.

All these misconceptions lead people to 'self-stigmatise', where people start taking others opinions in and second-guessing themselves, becoming XXX

$$\\[0.1em]$$

It's been a couple of months and Sarah's sort of treating it like a dark comedy. "The psychosis was terrifying, but at least it went away. I guess I've just gotta show everyone I'm normal."

"Have you thought about leaving?

"Yeah every day haha"


two thoughts
	everyone else does it
	stopping means we have a problem
	stopping means that we've forever labelled as an addict

		rationalise
			everyone does it!
			benefits outweigh the risks
			(but since nobody's brain can handle porn isn't everyone addicted?)


	"I have a problem"
		your addicted
		treatment
		streak counting
		hard
		fail
		shame
		reinforced
			others
				"isn't really a problem for me"
				online quitting communities
