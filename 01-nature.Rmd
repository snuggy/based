# Nature

Pornography is addictive because each new image or video keeps dopamine around in your brain for much longer than you can handle. With the internet, we can now view more potential partners in 15 minutes than our ancestors could in several lifetimes. Constantly pumping lots of dopamine short-circuits our brains, leading to desensitising and escalating behaviour.

But this book isn't about that.

If you're anything like I was, awareness of the health risks hasn't helped you one bit. You've done hours of reading on the effects, you understand the biological process inside and out. You've tried educating your friends to no avail, only to find yourself on a porn site a couple of days later.

However, to those I'm describing, I ask you put yourself in the position of someone who's never heard about supernormal stimulus -- after all, they might be reading right now. Try vividly remembering your first ever porn session; imagining that as the site opened, you begun receiving a cosmic transmission describing what was occuring.

### Porn site homepage

Catching sight of the endless number of different videos and images, each varying in quality, nationality and content feels overwhelming. Your mindset starts changing as your primal limbic system immediately kicks into gear. Built for foraging, fighting and fornicating, it's found the jackpot of potential mates, all mere keystrokes away.

You're feeling slightly glued to the screen because your brain is releasing huge amounts of dopamine in response to novelty.

Dopamine is a neurotransmitter inspiring action. An typical example would be hunger -- your body signalling to your brain that you're requiring fuel. Like endless generations before you, you expereince less dopamine in your limbic system, letting your consciousness know somethings up. Recognising the feeling as hunger, you begin heading to the kitchen, and as you're doing so, your limbic system is releasing little spurts of dopamine telling you that you're on the right track.

As you bite into what you've prepared, opioids flood your brain; rewarding you for a job well done.

You're already repeated this tasty journey thousands of times. You're remembering it because of a chemical called Delta-FosB that's released alongside dopamine strengthening neural pathways and linking neurons together. If your neurons themselves could talk, they'd say:

*"When you're feeling hungry getting something to eat feels good, so we're wiring the neurons involved in this together because not being hungry is important, and we know solving it like this works well."*

Your limbic system is a very important part of your brain. Without it, you wouldn't feel motivated doing anything.

But as mentioned, your limbic system isn't just for feeding. On the homepage, one of your key biological impulses has been activated. It's really powerful, and there's a reason people go to all-you-can-eat buffets; novelty keeps your poor limbic chasing rewards far longer than would be possible.

At a buffet, you eventually get full. Porn has no such moderator.

This is your first session, and many of the videos aren't to your taste. Looking at the suggested tags, you're surprised people even *enjoy* some of them. Some are shocking. Some are violent. Some, you fear would make you physically ill if you clicked on them. So, opting for a 'safe' clip, you pull down your pants and begin using.

Viewing the clip instantly delivers dopamine and opioids 'rewarding' you for finding something suited to your tastes. But this rollercoster must come down, and you begin reacting to the clip with mild boredom. No matter, the site's algorithms are suggesting an infinite sprawling novelty rabbit holes, and you repeat the process a couple of times until you eventually climax, sending massive amounts of opioids churning through your brain.

So too, Delta-FosB; now your neurons are saying:

*"eeeUaHeEEEHAhAAAHHHHHHHH WHATWHAT WHAT wHat the HECK what the heck are you joking what the heck repeat that repeat that how do you do that write that down that's important. when you get horny, go to the computer and go to the website and go to the videos and masturbate and climax. go go go can we do it again right now can we do it again"*

Your brain wasn't built for this -- the same circuitry driving you to flirt, seduce, foreplay and make love to someone dear to you about has been cruelly hijacked for a supernormal release of dopamine, often before we've tasted anything else.

Much to your limbic system's disappointment, because your brain has released lots of dopamine, it's released lots of prolactin too -- a chemical making you less sensitive to dopamine and opioids because (fun fact) during sex, it's better taking a break for increasing the odds of fertilisation.

But calling what you've just done "sex" is insulting to the act of lovemaking. What you've experienced is a highly addictive drug.

Sitting breathless, alone, in a pool of your own sweat, staring at a screen with something that used to be highly interesting, but is now slightly repuslive. You quickly close the window.

The whole process looks a little something like this.

[chart] -- girl drop, girl drop, girl drop, spike, big drop

But the ride isn't over yet, because you've just released massive amounts of dopamine, your brain now has low amounts of it. You're now in chemical withdrawal. At this stage of your newly formed dopamine based addiction, the withdrawal is subtle, but it is there.

You're feeling lethargic because of orgasm, but the dopaminergic processes that the limbic system controls are also affected; you're now chasing short-term dopamine releases, like those of sugar, social media, caffeine, and methamphetamine.

But most importantly, your limbic system is chasing that insane path your Delta-FosB has reinforced. Over time, as the prolactin inhibiting release begins to fade, your brain starts returning to a normal level of dopamine. But the withdrawal is still there, you're not *quite* at the level you were when you first started.

[bar graph] -- before started using, where you are now.

Over time, your biological urges, alongside the (currently) subtle undertone of withdrawal, push you into trying to get that rush again -- trying to return to the state of peace and tranquility before you first started -- so you jump onto your computer for another go at your personal, immediate, infinite porn 'paradise'.

The same clips you used before don't have the same effect, because the little dopamine withdrawal monster wants *novelty*. Your limbic system has been desensitised to the previous clips, so you require more and more porn for getting the same effect, and as you have more and more sessions, whole genres stop 'doing it for you', so without thinking twice about it, you click on the shocking clip you swore that you wouldn't when you first started. All to feed the little monster.

The whole process looks a little something like this:

[graphic of waning dopamine sessions over time under dopamine baseline, then escalating into unsafe and shocking material]

Porn is a desensitising, escalating, dopamine based addiction. You don't need to understand the process behind it any more than a cat has to understand where the hot water pipes are. The cat just knows that when it sits in a certain spot it feels warm. XXX PARAPHRASE CAT

Porn is a desensitising, escalating, dopamine based addiction. Understanding it isn't needed; a cat doesn't have understand where the hot water pipes are hidden, the cat just knows that sitting in certain spots makes it feels warm.

At it's core, the trap is a confidence trick. Most of the clips on any porn site are of homemade amaturish clips -- indeed, porn sites encourage their users to become 'stars' -- so the user is reassured they'll never become hooked.

In this cycle, you triggered feelings of

[chapter 4.2]
